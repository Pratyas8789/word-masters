if (!navigator.onLine) {
    alert("Sorry your internet is not working!! please try again. ")
} else {

    let letters = document.getElementsByClassName("box")

    let done = false;
    async function init(params) {
        let currentletter = '';
        let rowindex = 0;

        const response = await fetch("https://words.dev-apis.com/word-of-the-day")
        const obj = await response.json();
        const word = obj.word.toUpperCase();
        const wordpart = word.split("")

        function addletter(letter) {
            if (currentletter.length < 5) {
                currentletter += letter;
            } else {
                currentletter = currentletter.substring(0, currentletter.length - 1) + letter;
            }
            letters[5 * rowindex + currentletter.length - 1].innerText = letter
        }

        function commit(params) {
            const map = makeMap(wordpart);



            if (currentletter.length == 5 && done == false) {

                const guesspart = currentletter.split("");

                for (let i = 0; i < 5; i++) {
                    if (guesspart[i] == wordpart[i]) {
                        letters[5 * rowindex + i].classList.add("correct");
                        map[guesspart[i]]--;
                    }
                }


                for (let i = 0; i < 5; i++) {
                    if (guesspart[i] == wordpart[i]) {

                    } else if (wordpart.includes(guesspart[i]) && map[guesspart[i]] > 0) {
                        letters[5 * rowindex + i].classList.add("guess");
                        map[guesspart[i]]--;
                    } else {
                        letters[5 * rowindex + i].classList.add("wrong");
                    }
                }
                rowindex++;

                if (currentletter === word) {
                    alert('you win')
                    done = true;
                    return;
                }
                currentletter = '';


                if (rowindex == 6) {
                    const word = obj.word.toUpperCase();
                    alert(`you lose this game, the word is ${word}`)
                    return;
                }
            }

        }

        function backspace(params) {
            currentletter = currentletter.substring(0, currentletter.length - 1);
            letters[5 * rowindex + currentletter.length].innerText = ""
        }


        document.addEventListener("keydown", function name(event) {
            const key = event.key
            if (done == false) {
                if (key === 'Enter') {
                    commit();
                } else if (key === 'Backspace') {
                    backspace();
                } else if (isLetter(key)) {
                    addletter(key.toUpperCase())
                } else {

                }
            }
        });
        function isLetter(letter) {
            return /^[a-zA-Z]$/.test(letter);
        }



    }
    function makeMap(array) {
        const obj = {}
        for (let i = 0; i < array.length; i++) {
            const letter = array[i]
            if (obj[letter]) {
                obj[letter]++;
            } else {
                obj[letter] = 1;
            }
        }
        return obj;
    }
    init();
}