# What is this word masters?
Word masters is a guessing game which contains only five letters of word. 

# Built With
* html
* css
* javascript
* api

# How to work this game?
* firstly you think five letters of word.
* Then you write this word then press enter.
* if the letter does not exist then this box is red. if the letter presents in right place then this box is green. if letter exists but do not on right place then this box is yellow.
* You will be having only 6 chances to guess the word . If you guess the right word in within this chances you win else you loose, and the result is displayed on the screen.

# Hosted link
* https://wordy-master.netlify.app/
